
++++
<div style="padding-bottom: 20px;">
<div style="border: 1px solid #e8e8e8; padding: 10px 10px; padding-bottom: 0px; border-radius: 3px;">
++++

--
Other posts in this thread:

{% assign thread_posts = site.posts | where: "thread",page.thread | sort: "date" %}
{% for post in thread_posts %}
{% if post == page %}
. {{ post.title }}
{% else %}
. link:{{ site.baseurl }}{{ post.url }}[{{ post.title }}]
{% endif %}
{% endfor %}
--

++++
</div>
</div>
++++
