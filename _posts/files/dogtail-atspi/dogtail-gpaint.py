#!/usr/bin/env python3

import dogtail.config
dogtail.config.config.runTimeout = 2

import dogtail.procedural

window_title = "Untitled - gpaint"
pid = dogtail.procedural.run("gpaint", appName=window_title)

app = dogtail.procedural.focus.application.node
win = app.window(window_title)
file_menu = win.menuItem("File")
open_menuitem = file_menu.menuItem("Open")

open_menuitem.doActionNamed("click")
dlg = app.dialog("Open image")

cancel_button = dlg.button("Cancel")
cancel_button.doActionNamed("click")

file_menu.menuItem("Quit").doActionNamed("click")
