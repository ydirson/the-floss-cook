#!/usr/bin/env python3

import dogtail.config
dogtail.config.config.runTimeout = 2

import dogtail.procedural
import os, signal

os.environ['QT_LINUX_ACCESSIBILITY_ALWAYS_ON'] = '1'

window_title = "Untitled"
pid = dogtail.utils.run("featherpad", appName="FeatherPad")

app = dogtail.procedural.focus.application.node
win = app.window(window_title)

win.menuItem("File").menuItem("Open").doActionNamed("Press")

dlg = app.dialog("Open file...")

cancel_button = dlg.button("Cancel")
cancel_button.doActionNamed("Press")

win.menuItem("File").menuItem("Quit").doActionNamed("Press")
