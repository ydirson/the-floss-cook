#!/usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

window = Gtk.Window()
window.connect("destroy", lambda w: Gtk.main_quit())
vbox = Gtk.VBox()
window.add(vbox)

chooser = Gtk.FileChooserWidget(
    action=Gtk.FileChooserAction.OPEN)
vbox.pack_start(chooser, expand=False, fill=True, padding=0)

btn = Gtk.Button(label="Press me")
vbox.pack_start(btn, expand=False, fill=False, padding=0)

window.show_all()
Gtk.main()
