#!/usr/bin/python3 -u
from gi.repository import GLib
import pyatspi
import sys

def _registryChildChanged(event):
    global the_app
    if event.sender is desktop:
        if event.type.major != "children-changed":
            return
        if (event.type.minor == "add"
            and event.any_data.getRoleName() == "application"
            and event.any_data.name == appname
        ):
            if the_app != None:
                print("Ignoring extra '%s' app" % appname)
                return
            print(f"App '{appname}' 0x0x{id(the_app):x} intercepted")
            the_app = event.any_data
            print("App windows: {}".format([str(w) for w in the_app]))
        elif event.type.minor == "remove":
            # WTF cannot make a link with recorded app ?
            if the_app not in list(desktop):
                print("App is gone")
                pyatspi.Registry.stop()
        else:
            print("DESKTOP EVENT: %s" % event)
    elif event.sender is the_app:
        if event.type.major != "children-changed":
            print(event)
            return
        assert event.sender is the_app
        print("APP CHANGED")
        print(event)
    elif event.sender in old_apps:
        pass # just ignore unrelated apps
    else:
        print(event)

appname = sys.argv[1]

desktop = pyatspi.Registry.getDesktop(0)
old_apps = list(desktop)
the_app = None

#GLib.timeout_add(200, pyatspi.Registry.pumpQueuedEvents)
pyatspi.Registry.registerEventListener(_registryChildChanged, 
                                       'object') # :children-changed

try:
    pyatspi.Registry.start(**{'async': True, 'gil': False})
except KeyboardInterrupt:
    pyatspi.Registry.stop()
