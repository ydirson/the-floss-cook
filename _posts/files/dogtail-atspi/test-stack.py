#!/usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

window = Gtk.Window()
window.connect("destroy", lambda w: Gtk.main_quit())

vbox = Gtk.VBox()
window.add(vbox)

stack = Gtk.Stack()
vbox.add(stack)

toggle = Gtk.ToggleButton(label="Switch")
vbox.add(toggle)

btn = Gtk.Label(label="off")
stack.add_named(btn, "off")
txt = Gtk.Label(label="on")
stack.add_named(txt, "on")

toggle.connect("toggled",
               lambda w: stack.set_visible_child_name("on" if w.get_active() else "off"))

window.show_all()
Gtk.main()
