#!/usr/bin/env python3

import dogtail.config
dogtail.config.config.runTimeout = 2

import dogtail.procedural
import os, signal

window_title = "Untitled Document 1 - gedit"
pid = dogtail.procedural.run("gedit", appName=window_title)

app = dogtail.procedural.focus.application.node
win = app.window(window_title)
open_button = win.button("Open")

open_button.doActionNamed("click")
dlg = app.child(name="Open", roleName="file chooser")

cancel_button = dlg.button("Cancel")
cancel_button.doActionNamed("click")

os.kill(pid, signal.SIGTERM)
