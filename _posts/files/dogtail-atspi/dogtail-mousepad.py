#!/usr/bin/env python3

import dogtail.config
dogtail.config.config.runTimeout = 2

import dogtail.procedural
import os, signal

window_title = "Untitled 1 - Mousepad"
pid = dogtail.procedural.run("mousepad", appName=window_title)

app = dogtail.procedural.focus.application.node
win = app.window(window_title)

win.menu("File").menuItem("Open...").doActionNamed("click")

dlg = app.child(roleName="file chooser")

cancel_button = dlg.button("Cancel")
cancel_button.doActionNamed("click")

os.kill(pid, signal.SIGTERM)
