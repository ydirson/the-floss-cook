#!/usr/bin/python3 -u
from gi.repository import GLib
import pyatspi
import datetime
import sys

def pretty_accessible(acc):
    if acc == None:
        return "NONE"
    if acc is desktop:
        return "DSK"

    app_str = pretty_app(acc.get_application())

    states = acc.get_state_set()
    if states.contains(pyatspi.STATE_DEFUNCT):
        return (f"[(DEAD){app_str}:{acc.path}]")

    # cannot even check role when it is dead <sigh>
    if acc.get_role() == pyatspi.ROLE_APPLICATION:
        return app_str

    role = acc.get_role()
    role_str = role.value_nick if role is not None else "?"
    name = acc.get_name()
    name_str = name if name is not None else "?"
    return (f"[{role_str}|{name_str}|{app_str}:{acc.path}]")

def pretty_app(app):
    if app == None:
        return "NONE"

    states = app.get_state_set()
    if states.contains(pyatspi.STATE_DEFUNCT):
        # we cannot use get_id() if it is dead, and then its id() seems
        # semi-random, but sometimes we can see its role+name
        return f"DEADAPP(0x{id(app):x}, {app})"

    assert app.get_role() == pyatspi.ROLE_APPLICATION
    if app is the_app:
        return "APP"

    return f"APP(0x{id(app):x}, {app.get_name()}, id={app.get_id()})"

def log_event(event):
    #print("event %s" % event)
    sender_str = pretty_accessible(event.sender)
    source_str = pretty_accessible(event.source)

    if isinstance(event.any_data, pyatspi.Accessible):
        any_data_str = pretty_accessible(event.any_data)
    else:
        any_data_str = str(event.any_data)

    ts = datetime.datetime.now().strftime('%H:%M:%S')
    print(f"{ts} {sender_str}: {event.type}({event.detail1}, {event.detail2}, {any_data_str}, src={source_str})")

def _registryChildChanged(event):
    global the_app
    if event.sender is desktop:
        if event.type.major != "children-changed":
            return
        if (event.type.minor == "add"
            and event.any_data.getRoleName() == "application"
            and event.any_data.name == appname
        ):
            if the_app != None:
                print("Ignoring extra '%s' app" % appname)
                return
            the_app = event.any_data
            print(f"App '{appname}' 0x0x{id(the_app):x} id={the_app.get_id()} intercepted")
            print("App windows: {}".format(' '.join(pretty_accessible(w) for w in the_app)))
        elif event.type.minor == "remove":
            # WTF cannot make a link with recorded app ?
            if the_app not in list(desktop):
                print("App is gone")
                log_event(event)
                pyatspi.Registry.stop()
        else:
            print("DESKTOP EVENT: %s" % event)
    elif event.sender is the_app:
        if event.type.major != "children-changed":
            log_event(event)
            return
        assert event.sender is the_app
        print("APP CHANGED")
        log_event(event)
    elif event.sender in old_apps:
        pass # just ignore unrelated apps
    else:
        log_event(event)


appname = sys.argv[1]

desktop = pyatspi.Registry.getDesktop(0)
old_apps = list(desktop)
the_app = None

#GLib.timeout_add(200, pyatspi.Registry.pumpQueuedEvents)
pyatspi.Registry.registerEventListener(_registryChildChanged, 
                                       'object') # :children-changed

try:
    pyatspi.Registry.start(**{'async': True, 'gil': False})
except KeyboardInterrupt:
    pyatspi.Registry.stop()
